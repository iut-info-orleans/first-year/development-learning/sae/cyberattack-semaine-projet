"""
Projet CyberAttack@IUT'O
SAÉ1.01 département Informatique IUT d'Orléans 2021-2022

Module test_plateau
ce module teste les fonctions d'implémentation du plateau.
"""
import plateau
import case


# Création de plateaux
def creer_quatre_plateaux():
    """
    Crée 4 plateaux pour les tests.
    """

    plateau_1 = plateau.creer_plateau(1, "Antoine", nb_points=6)
    plateau_2 = plateau.creer_plateau(2, "Kylian", nb_points=3)
    plateau_3 = plateau.creer_plateau(3, "Romain", nb_points=7)
    plateau_4 = plateau.creer_plateau(4, "Jean-Baptiste",5,0,49)


    return (plateau_1, plateau_2, plateau_3, plateau_4)


# Fonctions de tests
def test_get_id_joueur():
    (plateau_1, plateau_2, plateau_3, plateau_4) = creer_quatre_plateaux()

    assert plateau.get_id_joueur(plateau_1) == 1
    assert plateau.get_id_joueur(plateau_2) == 2
    assert plateau.get_id_joueur(plateau_3) == 3
    assert plateau.get_id_joueur(plateau_4) == 4


def test_get_nom_joueur():
    (plateau_1, plateau_2, plateau_3, plateau_4) = creer_quatre_plateaux()

    assert plateau.get_nom_joueur(plateau_1) == "Antoine"
    assert plateau.get_nom_joueur(plateau_2) == "Kylian"
    assert plateau.get_nom_joueur(plateau_3) == "Romain"
    assert plateau.get_nom_joueur(plateau_4) == "Jean-Baptiste"


def test_get_trojans_entrants():
    (plateau_1, plateau_2, plateau_3, plateau_4) = creer_quatre_plateaux()

    assert plateau.get_trojans_entrants(plateau_1) == []
    assert plateau.get_trojans_entrants(plateau_2) == []
    assert plateau.get_trojans_entrants(plateau_3) == []
    assert plateau.get_trojans_entrants(plateau_4) == []


def test_get_taille():
    (plateau_1, plateau_2, plateau_3, plateau_4) = creer_quatre_plateaux()

    assert plateau.get_taille(plateau_1) == 5
    assert plateau.get_taille(plateau_2) == 5
    assert plateau.get_taille(plateau_3) == 5
    assert plateau.get_taille(plateau_4) == 5


def test_get_trojans_sortants():
    (plateau_1, plateau_2, plateau_3, plateau_4) = creer_quatre_plateaux()

    assert plateau.get_trojans_sortants(plateau_1, "H") == []
    assert plateau.get_trojans_sortants(plateau_2, "B") == []
    assert plateau.get_trojans_sortants(plateau_3, "H") == []
    assert plateau.get_trojans_sortants(plateau_4, "G") == []


def test_get_case():
    (plateau_1, plateau_2, plateau_3, plateau_4) = creer_quatre_plateaux()
    une_case = case.creer_case("", None, None, [])

    assert plateau.get_case(plateau_1, 0, 3) == une_case
    assert plateau.get_case(plateau_2, 2, 0) == une_case
    assert plateau.get_case(plateau_3, 1, 3) == une_case
    assert plateau.get_case(plateau_4, 3, 3) == une_case


def test_get_points():
    (plateau_1, plateau_2, plateau_3, plateau_4) = creer_quatre_plateaux()

    assert plateau.get_points(plateau_1) == 6
    assert plateau.get_points(plateau_2) == 3
    assert plateau.get_points(plateau_3) == 7
    assert plateau.get_points(plateau_4) == 0

def test_get_nb_ordis_restants():
    (plateau_1, plateau_2, plateau_3, plateau_4) = creer_quatre_plateaux()

    assert plateau.get_nb_ordis_restants(plateau_1) == 5
    assert plateau.get_nb_ordis_restants(plateau_1) == 5
    assert plateau.get_nb_ordis_restants(plateau_1) == 5
    assert plateau.get_nb_ordis_restants(plateau_1) == 5

def test_get_pos_avatar():
    (plateau_1, plateau_2, plateau_3, plateau_4) = creer_quatre_plateaux()

    assert plateau.get_pos_avatar(plateau_1) == (2,2)
    assert plateau.get_pos_avatar(plateau_2) == (2,2)
    assert plateau.get_pos_avatar(plateau_3) == (2,2)
    assert plateau.get_pos_avatar(plateau_4) == (2,2)

    plateau.poser_avatar(plateau_3,2,1)
    plateau.poser_avatar(plateau_1,0,0)

    assert plateau.get_pos_avatar(plateau_1) == (0,0)
    assert plateau.get_pos_avatar(plateau_3) == (2,1)

def test_poser_protection():
    (plateau_1, plateau_2, plateau_3, plateau_4) = creer_quatre_plateaux()

    assert plateau.get_pos_avatar(plateau_1) == (2,2)
    assert plateau.get_pos_avatar(plateau_2) == (2,2)
    assert plateau.get_pos_avatar(plateau_3) == (2,2)
    assert plateau.get_pos_avatar(plateau_4) == (2,2)

    plateau.poser_avatar(plateau_3,2,1)
    plateau.poser_avatar(plateau_1,0,0)

    assert plateau.get_pos_avatar(plateau_1) == (0,0)
    assert plateau.get_pos_avatar(plateau_3) == (2,1)

def test_ajouter_points():
    (plateau_1, plateau_2, plateau_3, plateau_4) = creer_quatre_plateaux()
    
    assert plateau.ajouter_points(plateau_1,5) == 11
    assert plateau.ajouter_points(plateau_4,5) == 5
    assert plateau.ajouter_points(plateau_2,-5) == -2

def test_attaquer_ordinateurs():
    (plateau_1, plateau_2, plateau_3, plateau_4) = creer_quatre_plateaux()

    assert plateau.attaquer_ordinateurs(plateau_1) == 5
    assert plateau.attaquer_ordinateurs(plateau_4) == 5