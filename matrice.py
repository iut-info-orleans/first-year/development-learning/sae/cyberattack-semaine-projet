"""
             Projet CyberAttack@IUT'O
        SAÉ1.01 département Informatique IUT d'Orléans 2021-2022

    Module matrice.py
    Ce module de gestion des matrices
"""


def creer_matrice(nb_lig, nb_col, val_defaut=None):
    """créer une matrice contenant nb_lig lignes et nb_col colonnes avec
       pour valeur par défaut val_defaut

    Args:
        nb_lig (int): un entier strictement positif
        nb_col (int): un entier strictement positif
        val_defaut (Any, optional): La valeur par défaut des éléments de la matrice.
                                    Defaults to None.
    Returns:
        dict: la matrice
    """
    matrice = dict()
    for lig in range(nb_lig):
        for col in range(nb_col):
            matrice[(lig, col)] = val_defaut
    return matrice


def get_nb_lignes(matrice):
    """retourne le nombre de lignes de la matrice

    Args:
        matrice (dict): une matrice

    Returns:
        int: le nombre de lignes de la matrice
    """
    max_lignes = None 
    for position in matrice: #Parcours de toutes les positions
        (lig, _) = position #Séparation du tuple
        if max_lignes is None or max_lignes < lig:
            max_lignes = lig
    return max_lignes + 1


def get_nb_colonnes(matrice):
    """retourne le nombre de colonnes de la matrice

    Args:
        matrice (dict): une matrice

    Returns:
        int: le nombre de colonnes de la matrice
    """
    max_colonnes = None
    for position in matrice:
        (_, col) = position
        if max_colonnes is None or max_colonnes < col:
            max_colonnes = col
    return max_colonnes + 1


def get_val(matrice, lig, col):
    """retourne la valeur en lig, col de la matrice

    Args:
        matrice (dict): une matrice
        lig (int): numéro de la ligne (en commençant par 0)
        col (int): numéro de la colonne (en commençant par 0)

    Returns:
        Any: la valeur en lig, col de la matrice
    """
    return matrice[(lig, col)]


def set_val(matrice, lig, col, val):
    """stocke la valeur val en lig, col de la matrice

    Args:
        matrice (dict): une matrice
        lig (int): numéro de la ligne (en commençant par 0)
        col (int): numéro de la colonne (en commençant par 0)
        val (Any): la valeur à stocker
    """
    matrice[(lig, col)] = val


def max_matrice(matrice, interdits=None):
    """retourne la liste des coordonnées des case contenant la valeur la plus grande de la matrice
        Ces case ne doivent pas être parmis les interdits

    Args:
        matrice (dict): une matrice
        interdits (set): un ensemble de tuples (ligne,colonne) de case interdites. Defaults to None

    Returns:
        list: les coordonnées de case de valeur maximale dans la matrice (hors cases interdites)
    """
    max_actuel = None
    coords_des_maxs = []
    if interdits is None:
        interdits = set()
    for lig in range(get_nb_lignes(matrice)):
        for col in range(get_nb_colonnes(matrice)):
            nouvelle_val = get_val(matrice, lig, col)
            # si la valeur actuelle plus grande et que coordonnées pas interdits
            if (max_actuel is None or max_actuel < nouvelle_val) and (lig, col) not in interdits:
                # On a trouvé un nouveau max -> on vide ce qu'on avait trouvé avant
                coords_des_maxs = []
                max_actuel = nouvelle_val
                coords_des_maxs.append((lig, col))
            # On retombe sur la même valeur -> on doit le prendre en compte si ses coordonnées pas interdites
            elif nouvelle_val == max_actuel and (lig, col) not in interdits:
                coords_des_maxs.append((lig, col))
    return coords_des_maxs



DICO_DIR = {(-1, -1): 'HG', (-1, 0): 'HH', (-1, 1): 'HD', (0, -1): 'GG',
            (0, 1): 'DD', (1, -1): 'BG', (1, 0): 'BB', (1, 1): 'BD'}


def direction_max_voisin(matrice, ligne, colonne):
    """retourne la liste des directions qui permette d'aller vers la case voisine de (ligne,colonne)
       la plus grande avec en plus la direction qui permet de se rapprocher du milieu de la matrice
       si ligne,colonne n'est pas le milieu de la matrice

    Args:
        matrice (dict): une matrice
        ligne (int): le numéro de la ligne de la case considérée
        colonne (int): le numéro de la colonne de la case considérée

    Returns:
        str: deux lettres indiquant la direction DD -> droite , HD -> Haut Droite,
                                                 HH -> Haut, HG -> Haut gauche,
                                                 GG -> Gauche, BG -> Bas Gauche, BB -> Bas
    """
    val_max = None
    direction_des_max = []
    #Parcours de toutes les directions
    for direction in DICO_DIR:
        nouv_lig = ligne + direction[0]
        nouv_col = colonne + direction[1]
        #Les nouvelles valeurs qu'on a selectionné doivent être compris dans la matrice
        if 0 <= nouv_lig < get_nb_lignes(matrice) and 0 <= nouv_col < get_nb_colonnes(matrice):
            nouv_val = get_val(matrice, nouv_lig, nouv_col)
            #Recherche de max en stockant la direction dans laquelle aller
            if val_max is None or val_max < nouv_val:
                direction_des_max = []
                val_max = nouv_val
                direction_des_max.append(DICO_DIR[direction])
            #Si max et valeur actuelle sont égaux, alors il faut ajouter la direction actuelle
            elif val_max is not None and nouv_val == val_max:
                direction_des_max.append(DICO_DIR[direction])
    return direction_des_max