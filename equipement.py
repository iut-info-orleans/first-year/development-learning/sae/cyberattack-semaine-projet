"""
             Projet CyberAttack@IUT'O
        SAÉ1.01 département Informatique IUT d'Orléans 2021-2022

    Module equipement.py
    ce module gère les équipements ordinateurs et serveurs
"""

SERVEUR = 0
ORDINATEUR = 1


def creer_equipement(type_e, resistance):
    """créer un équipement avec resistance

    Args:
        type_e (int): type de l'équipement
        resistance (int): le nombre d'attaques nécessaires pour détruire l'équipement

    Returns:
        dict: une structure représentant l'équipement
    """
    res = dict()
    res['type_e'] = type_e
    res['resistance'] = resistance
    return res


def attaque(equipement):
    """Enlève une protection à l'équipement

    Args:
        equipement (dict): un équipement
    """
    nv_resistance = get_resistance(equipement) - 1 #Variable nv_resistance sera la nouvelle resistance si elle est positive
    if nv_resistance < 0:
        return 0
    else:
        set_resistance(equipement, nv_resistance)
        return get_resistance(equipement)

def est_detruit(equipement):
    """Indique si l'équipement est détruit (n'a plus de resistance)

    Args:
        equipement (dict): un équipement

    Returns:
        bool: True si l'équipement n'a plus de résistance et False sinon
    """
    if equipement['resistance'] == 0:
        return True
    return False


def get_resistance(equipement):
    """retourne la resistance de l'équipement

    Args:
        equipement (dict): un équipement

    Returns:
        int: la resistance restante de l'équipement
    """
    return equipement['resistance']


def get_type(equipement):
    """retourne le type de l'équipement

    Args:
        equipement (dict): un équipement

    Returns:
        int: le type de l'équipement
    """
    return equipement['type_e']


def set_resistance(equipement, resistance):
    """positionne la résistance d'un équipement à une valeur donnée

    Args:
        equipement (dict): un équipement
        resistance (int): la résistance restante de l'équipement
    """
    equipement['resistance'] = resistance
