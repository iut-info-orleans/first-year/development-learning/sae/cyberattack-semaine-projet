"""
Projet CyberAttack@IUT'O
SAÉ1.01 département Informatique IUT d'Orléans 2021-2022

Module equipement.py
ce module gère le plateau des joueurs  équipements ordinateurs et serveurs
"""
import matrice
import case
import equipement
import trojan
import protection
import joueur
from math import floor

# direction de déplacement possible pour l'avatar
DIRECTIONS_AVATAR = {"DD", "GG", "HH", "BB", "HD", "HG", "BD", "BG"}

# A AJOUTER DANS VOTRE FICHIER
# points par action
PENALITE = -5
TROJAN_ELIMINE = 10
TROJAN_PIEGE = 5
DEPLACEMENT_TROJAN = 1
EQUIPEMENT_TOUCHE = -15
# FIN AJOUT


def creer_plateau(
    id_joueur,
    nom_joueur,
    taille_plateau=5,
    resistance_serveur=40,
    resistance_pc=50,
    nb_points=0,
):
    """créer un nouveau plateau

    Args:
        id_joueur (int): l'identifiant du joueur à qui appartient le plateau
        nom_joueur (str): le nom du joueur à qui appartient le plateau
        taille_plateau (int, optional): la taille du plateau. Defaults to 5.
        resistance_serveur (int, optional): la resistance initiale du serveur. Defaults to 4.
        resistance_pc (int, optional): la resistance initiale des PC. Defaults to 5.
        nb_points (int,optional): le nombre de points du joueur. Defaults to 0.
    Returns:
        dict: un plateau tel que décrit dans le sujet
    """

    plateau = dict()
    matrice_plateau = matrice.creer_matrice(taille_plateau, taille_plateau)
    serveur = equipement.creer_equipement(equipement.SERVEUR, resistance_serveur)
    for lig in range(taille_plateau):
        for col in range(taille_plateau):

            une_case = case.creer_case("", None, None, [])

            if lig == (taille_plateau // 2) - 2 and col == taille_plateau // 2:
                case.set_fleche(une_case,'B')
            if lig == (taille_plateau // 2) - 1 and col == taille_plateau // 2:
                case.set_fleche(une_case,'B')
            if lig == (taille_plateau // 2) + 1 and col == taille_plateau // 2:
                case.set_fleche(une_case,'H')
            if lig == (taille_plateau // 2) + 2 and col == taille_plateau // 2:
                case.set_fleche(une_case,'B')
            if lig == taille_plateau // 2 and col == (taille_plateau // 2) - 1:
                case.set_fleche(une_case,'D')
            if lig == taille_plateau // 2 and col == (taille_plateau // 2) + 1:
                case.set_fleche(une_case,'G')
                


            if lig == taille_plateau // 2 and col == taille_plateau // 2:
                case.set_serveur(une_case,serveur)
                case.poser_avatar(une_case)
                

            matrice.set_val(matrice_plateau, lig, col, une_case)

    plateau["matrice"] = matrice_plateau
    plateau["joueur"] = joueur.creer_joueur(id_joueur, nom_joueur, nb_points)
    plateau["serveur"] = equipement.creer_equipement(equipement.SERVEUR, resistance_serveur)
    plateau["pc"] = equipement.creer_equipement(equipement.ORDINATEUR, resistance_pc)
    plateau["trojans_entrants"] = []
    plateau["trojans_sortants"] = {} 

    return plateau


def get_id_joueur(plateau):
    """retourne l'identifiant du joueur à qui appartient le plateau

    Args:
        plateau (dict): un plateau

    Returns:
        int: l'identifiant du joueur
    """

    return joueur.get_id(plateau["joueur"])


def get_nom_joueur(plateau):
    """retourne le nom du joueur à qui appartient le plateau

    Args:
        plateau (dict): un plateau

    Returns:
        str: le nom du joueur
    """

    return joueur.get_nom(plateau["joueur"])


def get_trojans_entrants(plateau):
    """retourne la liste des trojans arrivants sur le plateau

    Args:
        plateau (dict): un plateau

    Returns:
        list: la liste des trojans arrivants sur le plateau
    """

    return plateau["trojans_entrants"]


def get_taille(plateau):
    """retourne la taille de la matrice du plateau

    Args:
        plateau (dict): un plateau

    Returns:
        int: la taille du plateau
    """

    return matrice.get_nb_lignes(plateau.get("matrice"))


def get_trojans_sortants(plateau, direction):
    """retourne la liste des trojans sortants du plateau pour la destination direction

    Args:
        plateau (dict): un plateau
        direction (str): une des quatre directions H, B, D, G

    Returns:
        list: la liste des trojans sortants du plateau vers la direction indiquée
    """

    try:
        trojans = plateau.get("trojans_sortants")[direction]
    except KeyError:
        trojans = []

    return trojans


def get_case(plateau, ligne, colonne):
    """retourne la case qui se trouve en ligne,colonne sur le plateau

    Args:
        plateau (dict): un plateau
        ligne (int): le numéro de la ligne
        colonne (int): le numéro de la colonne

    Returns:
        dict: une case
    """

    return matrice.get_val(plateau.get("matrice"), ligne, colonne)


def get_points(plateau):
    """retourne le nombre de points de joueur à qui appartient le plateau

    Args:
        plateau (dict): un plateau

    Returns:
        int: le nombre de points du joueur
    """

    return joueur.get_points(plateau["joueur"])


def get_nb_ordis_restants(plateau):
    """indique combien il reste d'ordinateurs sur le plateau

    Args:
        plateau (dict): un plateau

    Returns:
        int: le nombre d'ordinateurs restants
    """

    ordis = plateau.get("pc")
    resistance_ordis = equipement.get_resistance(ordis)

    return floor(resistance_ordis / 10)


def get_resistance_serveur(plateau):
    """indique combien de resistance il reste au serveur

    Args:
        plateau (dict): un plateau

    Returns:
        int: le niveau de resistance restant au serveur
    """

    return equipement.get_resistance(plateau["serveur"])


def get_serveur(plateau):
    """retourne le serveur du plateau

    Args:
        plateau (dict): un plateau

    Returns:
        dict: un équipement de type serveur
    """

    return plateau["serveur"]


def get_pos_avatar(plateau):
    """retourne la position de l'avatar sur le plateau

    Args:
        plateau (dict): un plateau

    Returns:
        (int,int): la ligne et la colonne où se trouve l'avatar sur le plateau
    """

    for lig in range(matrice.get_nb_colonnes(plateau['matrice'])):
        for col in range(matrice.get_nb_lignes(plateau['matrice'])):
            if case.contient_avatar(plateau["matrice"][(lig, col)]):
                return (lig, col)

    return None


def get_ordis(plateau):
    """retourne les ordinateurs du plateau

    Args:
        plateau (dict): un plateau

    Returns:
        dict: un équipement de type ordinateurs
    """

    return plateau["pc"]


def get_matrice(plateau):
    """retourne la matrice associée au plateau

    Args:
        plateau (dict): un plateau

    Returns:
        dict: une matrice
    """

    return plateau["matrice"]

def poser_avatar(plateau, ligne, colonne):
    """pose l'avatar sur la case en ligne,colonne

    Args:
        plateau (dict): un plateau
        ligne (int): le numéro de la ligne
        colonne (int): le numéro de la colonne
    """

    la_case = get_case(plateau, ligne, colonne)
    case.poser_avatar(la_case)


def poser_protection(plateau, type_protection, ligne, colonne):
    """pose une protection sur le plateau avec les règles suivantes:
        1. aucune autre protection n'est sur la case
        2. aucun trojan ne se trouve sur la case où on mets la protection
        3. la ligne et la colonne existent
      La fonction retourne True si la pose de la protection s'est bien passée

    Args:
        plateau (dict): un plateau
        type_protection (int): le type de la protection à mettre
        ligne (int): le numéro de la ligne
        colonne (int): le numéro de la colonne

    Returns:
        bool: True si la pose s'est bien passée et False sinon

    """
    if 0 <= ligne < get_taille(plateau) and 0 <= colonne < get_taille(plateau):
        la_case = get_case(plateau, ligne, colonne)   
        if case.get_protection(la_case) == None:
            if case.get_trojans(la_case) == []:
                protect = protection.creer_protection(type_protection,protection.RESISTANCE)
                case.set_protection(la_case,protect)
                return True
    return False

def set_trojans_entrants(plateau, les_trojans):
    """Met les trojans entrants sur le plateau

    Args:
        plateau (dict): un plateau
        les_trojans (list): une liste de trojans
    """

    plateau['trojans_entrants'] = les_trojans

def set_trojans_sortants(plateau, direction, les_trojans):
    """mets les trojans devant sortir vers la direction indiquée

    Args:
        plateau (dict): un plateau
        direction (str): une des directions H, B, D, G
        les_trojans (list): une liste de trojans
    """

    plateau['trojans_sortants'][direction] = les_trojans



# FIN DES FONCTIONS NECESSAIRES POUR LA SAUVEGARDE/RESTAURATION D'UN PLATEAU


def id_joueur_droite(plateau):
    """retourne l'identifiant du joueur de droite

    Args:
        plateau (dict): un plateau

    Returns:
        int: l'identifiant du joueur de droite
    """

    return joueur.id_joueur_droite(plateau["joueur"])


def id_joueur_gauche(plateau):
    """retourne l'identifiant du joueur de gauche

    Args:
        plateau (dict): un plateau

    Returns:
        int: l'identifiant du joueur de gauche
    """

    return joueur.id_joueur_gauche(plateau["joueur"])


def id_joueur_haut(plateau):
    """retourne l'identifiant du joueur du haut

    Args:
        plateau (dict): un plateau

    Returns:
        int: l'identifiant du joueur du haut
    """

    return joueur.id_joueur_haut(plateau["joueur"])


def ajouter_points(plateau, nb_points):
    """ajoute le nombre de point nb_points au joueur du plateau

    Args:
        plateau (dict): un plateau
        nb_points (int): le nombre de points (éventuellement négatif)

    Returns:
        int: le nombre de points total du joueur du plateau
    """

    joueur.ajouter_points(plateau["joueur"], nb_points)

    return joueur.get_points(plateau["joueur"])


def ajouter_trojan(plateau, liste_trojan):
    """ajoute une liste de nouveaux trojan à la liste des trojan qui vont entrer
       sur le plateau lors du prochain tour

    Args:
        plateau (dict): un plateau
        liste_trojan (list): la liste des trojans à ajouter
    """

    plateau['trojans_entrants'] = liste_trojan


def attaquer_ordinateurs(plateau):
    """enlève une resistance aux ordinateurs du plateau

    Args:
        plateau (dict): un plateau

    Returns:
        int: le nombre d'ordinateurs restants
    """

    nombre_ordinateur_restant = 0 
    ordis = get_ordis(plateau)

    if equipement.get_resistance(ordis) != 0:
        equipement.set_resistance(ordis,equipement.get_resistance(get_ordis(plateau)) - 1)
    if equipement.get_resistance(ordis) == 0:
        nombre_ordinateur_restant = 0
    else:
        nombre_ordinateur_restant = get_nb_ordis_restants(plateau)+1
    return nombre_ordinateur_restant

def a_perdu(plateau):
    """indique si le joueur de ce plateau a perdu

    Args:
        plateau (dict): un plateau

    Returns:
        bool: True si le plateau est perdant et False sinon
    """

    return get_resistance_serveur(plateau) == 0 and get_nb_ordis_restants(plateau) == 0


def deplacer_avatar(plateau, direction):
    """déplace l'avatar dans une des directions 'DD', 'GG', 'HH', 'BB', 'HD', 'HG', 'BD', 'BG'
       Le joueur sera pénalisé de 5 points si l'avatar ne peut pas se déplacer
       dans la direction indiquée et il gagnera 5 points par trojans détruits

    Args:
        plateau (dict): un plateau
        direction (str): la direction vers où se déplace l'avatar
    """

    pos_avatar = get_pos_avatar(plateau)
    point_actu = get_points(plateau)

    if direction == 'DD':
        if pos_avatar[1] + 1 < get_taille(plateau):
            poser_avatar(plateau,pos_avatar[0],pos_avatar[1] + 1)
            la_case = get_case(plateau,pos_avatar[0],pos_avatar[1] + 1)
            ajouter_points(plateau,5 * (len(case.get_trojans(la_case))))

    if direction == 'GG':
        if pos_avatar[1] - 1 >= 0:
            poser_avatar(plateau,pos_avatar[0],pos_avatar[1] - 1)
            la_case = get_case(plateau,pos_avatar[0],pos_avatar[1] - 1)
            ajouter_points(plateau,5 * (len(case.get_trojans(la_case))))

    if direction == 'HH':
        if pos_avatar[0] - 1 >= 0:
            poser_avatar(plateau,pos_avatar[0] - 1,pos_avatar[1])
            la_case = get_case(plateau,pos_avatar[0] - 1,pos_avatar[1])
            ajouter_points(plateau,5 * (len(case.get_trojans(la_case))))

    if direction == 'BB':
        if pos_avatar[0] + 1 < get_taille(plateau):
            poser_avatar(plateau,pos_avatar[0] + 1,pos_avatar[1])
            la_case = get_case(plateau,pos_avatar[0] + 1,pos_avatar[1])
            ajouter_points(plateau,5 * (len(case.get_trojans(la_case))))

    if direction == 'HD':
        if pos_avatar[0] - 1 >= 0 and pos_avatar[1] + 1 < get_taille(plateau):
            poser_avatar(plateau,pos_avatar[0] - 1,pos_avatar[1] + 1)
            la_case = get_case(plateau,pos_avatar[0] - 1,pos_avatar[1] + 1)
            ajouter_points(plateau,5 * (len(case.get_trojans(la_case))))

    if direction == 'HG':
        if pos_avatar[0] - 1 >= 0 and pos_avatar[1] - 1 >= 0:
            poser_avatar(plateau,pos_avatar[0] - 1,pos_avatar[1] - 1)
            la_case = get_case(plateau,pos_avatar[0] - 1,pos_avatar[1] - 1)
            ajouter_points(plateau,5 * (len(case.get_trojans(la_case))))

    if direction == 'BD':
        if pos_avatar[0] + 1 < get_taille(plateau) and pos_avatar[1] + 1 < get_taille(plateau):
            poser_avatar(plateau,pos_avatar[0] + 1,pos_avatar[1] + 1)
            la_case = get_case(plateau,pos_avatar[0] - 1,pos_avatar[1] + 1)
            ajouter_points(plateau,5 * (len(case.get_trojans(la_case))))
    
    if direction == 'BG':
        if pos_avatar[0] + 1 < get_taille(plateau) and pos_avatar[1] - 1 >= 0:
            poser_avatar(plateau,pos_avatar[0] + 1,pos_avatar[1] - 1)
            la_case = get_case(plateau,pos_avatar[0] - 1,pos_avatar[1] - 1)
            ajouter_points(plateau,5 * (len(case.get_trojans(la_case))))

    if point_actu == get_points(plateau):
        plateau['joueur']['nb_points'] -= 5


def preparer_trojan(plateau, destinataire, type_t):
    """creer un nouveau trojan appartenant au joueur du plateau, à destination d'un des voisins
        et avec un type défini

    Args:
        plateau (dict): un plateau
        destinataire (str): une des direction D, G ou H
        type (int): un nombre entre 0 et taille-1
    """
    nv_trojan = dict()

    nv_trojan['createur'] = get_id_joueur(plateau)
    nv_trojan['type_t'] = type_t
    nv_trojan['direction'] = destinataire

    return nv_trojan


def diriger_trojan(plateau):
    """Dirige tous les trojan adjascents à des données personnelles vers ces données personnelles

    Args:
        plateau (dict): un plateau
    """

    for lig in range(get_taille(plateau)):
        for col in range(get_taille(plateau)):
            if get_case(plateau,lig,col)["la_protection"]["type_p"] == protection.DONNEES_PERSONNELLES:
                if case.get_trojans(get_case(plateau,lig + 1,col)):
                    trojans_presents = case.get_trojans(get_case(plateau,lig + 1,col))
                    for trojans in trojans_presents:
                        trojan.set_direction(trojans,'H')
                if case.get_trojans(get_case(plateau,lig - 1,col)):
                    trojans_presents = case.get_trojans(get_case(plateau,lig - 1,col))
                    for trojans in trojans_presents:
                        trojan.set_direction(trojans,'B')
                if case.get_trojans(get_case(plateau,lig ,col + 1)):
                    trojans_presents = case.get_trojans(get_case(plateau,lig,col + 1))
                    for trojans in trojans_presents:
                        trojan.set_direction(trojans,'G')
                if case.get_trojans(get_case(plateau,lig ,col - 1)):
                    trojans_presents = case.get_trojans(get_case(plateau,lig,col - 1))
                    for trojans in trojans_presents:
                        trojan.set_direction(trojans,'D')


def get_joueur(plateau):
    """retourne le joueur à qui appartient le plateau

    Args:
        plateau (dict): un plateau

    Returns:
        dict: un joueur
    """
    return plateau['joueur']                   

# FIN DES FONCTIONS A IMPLEMENTER AVANT SEMAINE DE PROJET
def deplacer_trojan_phase1(plateau):
    """déplace les chevaux de Troie présents sur les cases en fonction de leur direction
       cette fonction introduit aussi les trojans entrants du plateau
       Si le déplacement provoque la sortie du trojan du plateau, ce trojans est placés
       dans les trojans sortants du plateau

    Args:
        plateau (dict): un plateau
    """

    ind = 0

    for lig in range(get_taille(plateau)):
        for col in range(get_taille(plateau)):
            la_case = get_case(plateau,lig,col)
            trojans = case.get_trojans(la_case)

            for troj in trojans:
                ind += 1
                direction = trojan.get_direction(troj)

                if direction == 'H':
                    pos_actu = (lig - 1,col)
                if direction == 'B':
                    pos_actu = (lig + 1,col)
                if direction == 'D':
                    pos_actu = (lig ,col + 1)
                if direction == 'G':
                    pos_actu = (lig,col - 1)
                if pos_actu[0] < 0 or pos_actu[1] >=get_taille(plateau):
                    set_trojans_sortants(plateau,direction,[troj])
                else:
                    nv_case = get_case(plateau,pos_actu[0],pos_actu[1])
                    case.set_les_trojans(nv_case,case.get_trojans(nv_case),[troj])
                    case.set_les_trojans(la_case, case.get_trojans(la_case).pop(ind),[])
                    

def appliquer_dpo(plateau, ligne_protection, colonne_protection, les_trojans):
    """Cette fonction applique l'effet de la protection Data Protection Officer
       posée en ligne_protection, colonne_protection à une liste de trojans

    Args:
        plateau (dict): un plateau
        ligne_protection (int): la ligne de la protection
        colonne_protection (int): la colonne de la protection
        les_trojans (list): une liste de trojans
    """

    for trojans in les_trojans:
        trojan.inverser_direction(trojans)
        direction = trojan.get_direction(trojans)
        if direction == 'H':
            pos_t = (ligne_protection - 1,colonne_protection)
        if direction == 'B':
            pos_t = (ligne_protection + 1,colonne_protection)
        if direction == 'D':
            pos_t = (ligne_protection,colonne_protection + 1)
        if direction == 'G':
            pos_t = (ligne_protection,colonne_protection - 1)
        nv_case = get_case(plateau,pos_t[0],pos_t[1])
        case.set_les_trojans(nv_case,[trojans],[])


def appliquer_firewall_bdhg(plateau, ligne_protection, colonne_protection, les_trojans):
    """Cette fonction applique l'effet de la protection firewall basdroit-hautgauche
       posée en ligne_protection, colonne_protection à une liste de trojans

    Args:
        plateau (dict): un plateau
        ligne_protection (int): la ligne de la protection
        colonne_protection (int): la colonne de la protection
        les_trojans (list): une liste de trojans
    """

    for trojans in les_trojans:
        trojan.changer_direction_angle_bdhg(trojans)
        nv_direction = trojan.get_direction(trojans)
        if nv_direction == 'H':
            pos_t = (ligne_protection,colonne_protection + 1)
        if nv_direction == 'B':
            pos_t = (ligne_protection,colonne_protection - 1)
        if nv_direction == 'D':
            pos_t = (ligne_protection + 1,colonne_protection)
        if nv_direction == 'G':
            pos_t = (ligne_protection - 1,colonne_protection)
        nv_case = get_case(plateau,pos_t[0],pos_t[1])
        case.set_les_trojans(nv_case,[trojans],[])
        



def appliquer_firewall_bghd(plateau, ligne_protection, colonne_protection, les_trojans):
    """Cette fonction applique l'effet de la protection firewall basgauche-hautdroit
       posée en ligne_protection, colonne_protection à une liste de trojans

    Args:
        plateau (dict): un plateau
        ligne_protection (int): la ligne de la protection
        colonne_protection (int): la colonne de la protection
        les_trojans (list): une liste de trojans
    """
    for trojans in les_trojans:
        trojan.changer_direction_angle_bghd(trojans)
        nv_direction = trojan.get_direction(trojans)
        if nv_direction == 'H':
            pos_t = (ligne_protection,colonne_protection - 1)
        if nv_direction == 'B':
            pos_t = (ligne_protection,colonne_protection + 1)
        if nv_direction == 'D':
            pos_t = (ligne_protection - 1,colonne_protection)
        if nv_direction == 'G':
            pos_t = (ligne_protection + 1,colonne_protection)
        nv_case = get_case(plateau,pos_t[0],pos_t[1])
        case.set_les_trojans(nv_case,[trojans],[])


def appliquer_antivirus(plateau,les_trojans):
    """Cette fonction applique l'effet de la protection antivirus
       posée en ligne_protection, colonne_protection à une liste de trojans

    Args:
        plateau (dict): un plateau
        les_trojans (list): une liste de trojans
    """
    for trojans in les_trojans:
        attaquant = trojan.get_createur(trojans)
        defenseur = joueur.get_id(plateau["joueur"])
        trojan.set_createur(trojans,defenseur)
        type = trojan.get_type(trojans)
        preparer_trojan(plateau,attaquant,type)
        plateau['trojans_sortants'] += trojans
    #Besoin de copier sur quelqu'un vite fait


def deplacer_trojan_phase2(plateau):
    """Finalise le déplacements des chevaux de Troies en appliquant les protections

    Args:
        plateau (dict): un plateau

    Returns:
        dict: un dictionnaire donnant pour chaque joueur le nombre de trojans restant sur le plateau
    """
    trojans_restant = {1:0, 2:0, 3:0, 4:0}

    for i in range(get_taille(plateau)):
        for j in range(get_taille(plateau)):
            if case.contient_avatar(get_case(plateau,i,j)):
                case.set_les_trojans(get_case(plateau,i,j),[],[])

            if get_case(plateau,i,j)["serveur"] != None and case.get_trojans(get_case(plateau,i,j)) != []:
                for trojans in (case.get_trojans(get_case(plateau,i,j))):
                    equipement.attaque(get_case(plateau,i,j)["serveur"])

            if get_case(plateau,i,j)["la_protection"]["type_p"] == protection.DPO:
                appliquer_dpo(plateau,i,j,case.get_trojans((i,j)))

            if get_case(plateau,i,j)["la_protection"]["type_p"] == protection.ANTIVIRUS:
                appliquer_antivirus(plateau,i,j,case.get_trojans((i,j)))

            if get_case(plateau,i,j)["la_protection"]["type_p"] == protection.FIREWALL_BDHG:
                appliquer_firewall_bdhg(plateau,i,j,case.get_trojans((i,j)))

            if get_case(plateau,i,j)["la_protection"]["type_p"] == protection.FIREWALL_BGHD:
                appliquer_firewall_bghd(plateau,i,j,case.get_trojans((i,j)))

            if case.get_trojans(get_case(plateau,i,j)) != []:
                for trojans in case.get_trojans(get_case(plateau,i,j)):
                    createur = trojan.get_createur(trojans)
                    trojans_restant[createur] += 1

    return trojans_restant

def reinit_les_sorties(plateau):
    """Reinitialise les sorties du plateau à vide

    Args:
        plateau (dict): un plateau
    """
    plateau["trojans_sortants"] = []


def nb_trojans_prochain(plateau):
    """fonction qui retourne une matrice indiquant combien de trojans se trouveront
       sur chaque case du plateau au prochain tour

    Args:
        plateau (dict): un plateau

    Returns:
        dict: une matrice d'entiers
    """
    matrice1 = matrice.creer_matrice(get_taille(plateau),get_taille(plateau),0)
    for x in range(get_taille(plateau)):
        for y in range(get_taille(plateau)):
            trojans_entrants = case.get_trojans_entrants(get_case(plateau,x,y))
            matrice.set_val(matrice1,x,y,len(trojans_entrants))
    return matrice1

def executer_ordres(plateau, ordres):
    """Exécute les ordres choisis par le joueur. Les ordres sont donnés sous la forme
       d'une chaine de caractères dont les deux premiers indique le déplacement de l'avatar
       le troisième caractère est
       soit un A pour une attaque
       soit un P pour une protection
       En cas d'attaque, les caractères suivants sont GxHyDz où
                    x y et z sont des chiffres entre 0 et 4 indiquant le numéro de la
                             ligne ou de la colonne où sera envoyé le trojan
       En cas de pose d'une protection les caractère suivants seront trois chiffre tlc où
                    t est le type de la protection
                    l la ligne où poser la protection
                    c la colonne où poser la protection
        Si la chaine est non conforme où l'action non permise le joueur perd 5 points.
        Notez que les points et les pénalités liés au déplacement de l'avatar sont gérés dans
        deplacer_avatar

    Args:
        plateau (dict): un plateau
        ordres (str): une chaine de caractères donnant les ordres du joueur
    """
    # la longueur la plus courte est de 6 (poser une protection) et la plus longue est de 9 (une attaque)
    if len(ordres) >= 6 and len(ordres) <= 9:
        # on récupère l'action à effectuer (P pour protection et A action)
        action = ordres[2]

        # on gère la mise de protection
        if action == 'P':
            # la longueur d'une instruction pour poser une protection est de 6
            if len(ordres) == 6:
                # gérer la pose d'une protection
                action_protection = ordres[3:]

                # on découpe l'instruction en fonction du type de protection, de la ligne et de la colonne
                (type_p, lig, col) = (action_protection[0], action_protection[1], action_protection[2])
                # on vérifie que les informations sont correctes
                try:
                    # on vérifie qu'on peut convertir la ligne et la colonne en int
                    lig = int(lig)
                    col = int(col)

                    # on vérifie que la protection qu'on souhaite poser est sur le plateau
                    assert 0 <= lig < get_taille(plateau)
                    assert 0 <= col < get_taille(plateau)

                    # si les tests passent alors on créer et on pose la protection
                    case_poser_protection = get_case(plateau, lig, col)

                    if case.get_protection(case_poser_protection) is None:
                        # si une protection du même type a déjà été posée, on ne peut pas la poser
                        type_protection_posee = set()
                        for lig in range(get_taille(plateau)):
                            for col in range(get_taille(plateau)):
                                une_case = get_case(plateau, lig, col)
                                une_protection = case.get_protection(une_case)
                                if une_protection is not None:
                                    type_protection_posee.add(protection.get_type(case.get_protection(une_case)))
                        
                        if type_p not in type_protection_posee:
                            # on peut placer la protection
                            protection_a_poser = protection.creer_protection(type_p, protection.RESISTANCE)
                            case.set_protection(case_poser_protection, protection_a_poser) 

                except AssertionError:
                    # si la protection qu'on veut poser n'est pas sur le plateau
                    ajouter_points(plateau, PENALITE)  
                except ValueError:
                    # si les valeurs ne sont pas bonnes (transitions str -> int)
                    ajouter_points(plateau, PENALITE)
            else:
                # on applique une pénalité
                ajouter_points(plateau, PENALITE)

        # on gère l'attaque
        elif action == 'A':
            # la longueur d'une instruction pour créer une attaque est de 9
            if len(ordres) == 9:
                # gérer l'attaque
                action_attaque = ordres[3:]
                if len(action_attaque) == 6:
                    attaque_gauche = action_attaque[:2]
                    attaque_haut = action_attaque[2:4]
                    attaque_droite = action_attaque[4:]

                    try:
                        # on vérifie que l'indicateur d'attaque est bon (G, H ou D)
                        assert attaque_gauche[0] == 'G'
                        assert attaque_haut[0] == 'H'
                        assert attaque_droite[0] == 'D'

                        # on vérifie que le type peut être converti en int
                        type_gauche = int(attaque_gauche[1])
                        type_haut = int(attaque_haut[1])
                        type_droit = int(attaque_droite[1])

                        # on vérifie que l'indicateur de ligne est compris dans la taille
                        assert 0 <= type_gauche < get_taille(plateau)
                        assert 0 <= type_haut < get_taille(plateau)
                        assert 0 <= type_droit < get_taille(plateau)
                        
                        # si les tests sont passés alors on peut créer les attaques
                        trojan_gauche = trojan.creer_trojan(get_joueur(plateau), attaque_gauche[1], attaque_gauche[0])
                        trojan_haut = trojan.creer_trojan(get_joueur(plateau), attaque_haut[1], attaque_haut[0])
                        trojan_droite = trojan.creer_trojan(get_joueur(plateau), attaque_droite[1], attaque_droite[0])

                        set_trojans_sortants(plateau, 'G', [trojan_gauche])
                        set_trojans_sortants(plateau, 'H', [trojan_haut])
                        set_trojans_sortants(plateau, 'D', [trojan_droite])

                    except AssertionError:
                        # si les informations saisies ne sont pas correctes
                        ajouter_points(plateau, PENALITE)
                    except ValueError:
                        ajouter_points(plateau, PENALITE)
                else:
                    # si la chaine de caractère qui décrit l'attaque n'est pas de 6, on applique une pénalité
                    ajouter_points(plateau, PENALITE)
            else:
                # si la longueur de la chaine qui décrit l'attaque n'est pas valide, on applique une pénalité
                ajouter_points(plateau, PENALITE)
        else:
            # si le caractère d'action n'est ni A ni P, alors on applique une pénalité
            ajouter_points(plateau, PENALITE)
        
        # on récupère le déplacement du personnage
        deplacement_perso = ordres[:2]

        # on effectue le déplacement (dans le cas d'un mauvais déplacement, une pénalité est automatiquement appliquée)
        deplacer_avatar(plateau, deplacement_perso)
    else:
        # si la taille de la chaine de caractère n'est pas valide, on applique une pénalité
        ajouter_points(plateau, PENALITE)


# FONCTIONS A RECOPIER DANS VOTRE FICHIER
# -------------------------------------------------------------#
# Fonctions pour la sauvegarde et la restoration d'un plateau #
# -------------------------------------------------------------#


def plateau_2_str(plateau, separateur=";"):
    """sauvegarde un plateau sous la forme d'une chaîne de caractères

    Args:
        plateau (dict): un plateau
        separateur (str, optional): le séparateur utilisé pour séparer les champs
                                    sur une même ligne. Defaults to ";".

    Returns:
        str: la chaîne caractères représentant le plateau
    """
    pos_av = get_pos_avatar(plateau)
    res = (
        "plateau"
        + separateur
        + str(get_taille(plateau))
        + "\n"
        + "joueur"
        + separateur
        + str(get_id_joueur(plateau))
        + separateur
        + get_nom_joueur(plateau)
        + separateur
        + str(get_points(plateau))
        + "\n"
        + "avatar"
        + separateur
        + str(pos_av[0])
        + separateur
        + str(pos_av[1])
        + "\n"
        + "serveur"
        + separateur
        + str(get_resistance_serveur(plateau))
        + "\n"
        + "ordinateurs"
        + separateur
        + str(get_nb_ordis_restants(plateau))
        + "\n"
    )
    res += "trojans entrants\n"
    for troj in get_trojans_entrants(plateau):
        res += (
            str(trojan.get_createur(troj))
            + separateur
            + str(trojan.get_type(troj))
            + separateur
            + str(trojan.get_direction(troj))
            + "\n"
        )
    res += "trojans sortants\n"
    for dest in "HBDG":
        res += dest + "\n"
        for troj in get_trojans_sortants(plateau, dest):
            res += (
                str(trojan.get_createur(troj))
                + separateur
                + str(trojan.get_type(troj))
                + separateur
                + str(trojan.get_direction(troj))
                + "\n"
            )
    res += "matrice\n"
    taille = get_taille(plateau)
    mat = get_matrice(plateau)
    for ligne in range(taille):
        for colonne in range(taille):
            la_case = matrice.get_val(mat, ligne, colonne)
            if (
                case.get_trojans(la_case) != []
                or case.get_trojans_entrants(la_case) != []
                or case.get_protection(la_case) is not None
            ):
                res += (
                    "case" + separateur + str(ligne) + separateur + str(colonne) + "\n"
                )
                protec = case.get_protection(la_case)
                if protec is None:
                    res += "protection\n"
                else:
                    res += (
                        "protection"
                        + separateur
                        + str(protection.get_type(protec))
                        + ";"
                        + str(protection.get_resistance(protec))
                        + "\n"
                    )
                res += "trojans presents\n"
                for troj in case.get_trojans(la_case):
                    res += (
                        str(trojan.get_createur(troj))
                        + separateur
                        + str(trojan.get_type(troj))
                        + separateur
                        + str(trojan.get_direction(troj))
                        + "\n"
                    )
                res += "trojans entrants\n"
                for troj in case.get_trojans_entrants(la_case):
                    res += (
                        str(trojan.get_createur(troj))
                        + separateur
                        + str(trojan.get_type(troj))
                        + separateur
                        + str(trojan.get_direction(troj))
                        + "\n"
                    )
    return res + "fin plateau\n"


def valider_mot_cle(champs, indice, mot_cle, types_champs):
    """fonction outil permettant de valider un mot clé dans une chaine représentant un plateau

    Args:
        champs (list): liste des champs trouvé sur la ligne
        indice (int): numéro de la ligne dans la chaîne (utilisé pour les message d'erreur)
        mot_cle (str): le mot clé recherché
        types_champs (list): une liste de booléens indiquant les champs qui doivent être
                             convertis en entiers

    Raises:
        Exception: si le nombre de champs ne correspond pas à la longeur des types de champs +1
        Exception: si le mot clé n'est pas le premier champs
        Exception: si une convertion en entier s'est mal passée

    Returns:
        list: la liste des champs associés au mot clé
    """
    if len(champs) != len(types_champs) + 1:
        raise Exception(
            "ligne "
            + str(indice)
            + ": comporte "
            + str(len(champs))
            + " champs au lieu de "
            + str(len(types_champs) + 1)
        )

    if champs[0] != mot_cle:
        raise Exception(
            "ligne "
            + str(indice)
            + ": ne commence pas par le mot clé '"
            + mot_cle
            + "'"
        )
    res = []
    for ind in range(len(types_champs)):
        if types_champs[ind]:
            try:
                res.append(int(champs[ind + 1]))
            except:
                raise Exception(
                    "ligne "
                    + str(indice)
                    + ": le "
                    + str(ind + 1)
                    + "eme champs devrait être un entier\n"
                    + str(champs)
                )
        else:
            res.append(champs[ind + 1])
    return res


def liste_trojans_from_str(lignes, ind_debut, separateur):
    """récupère une liste de trojans dans les lignes à partir de ind_début

    Args:
        lignes (list): une liste de chaines de caractères correspondant à
            des lignes de la chaines représentant un plateau
        ind_debut (int): indice dans la liste lignes à partir de laquelle
            on recherche les trojans
        separateur (str): le caractère séparateur des champs dans une ligne

    Returns:
        int,list: le numéro de la ligne où s'arrête la liste de trojans, la
                  liste des trojans
    """
    res = []
    num_lig = ind_debut
    while True:
        champs = lignes[num_lig].split(separateur)
        if len(champs) != 3:
            break
        try:
            res.append(trojan.creer_trojan(int(champs[0]), int(champs[1]), champs[2]))
        except:
            break
        num_lig += 1
    return num_lig, res


def creer_plateau_from_str(chaine, separateur=";"):
    """recréer un plateau à partir d'une chaine de caractères

    Args:
        chaine (str): la chaine de caractère représentant le plateau
        separateur (str, optional): le caractère séparateur des champs au sein d'une ligne.
                                    Defaults to ';'.

    Raises:
        Des exceptions sont levées en cas d'erreur de format de la chaine d'entrée

    Returns:
        dict: le plateau correcpondant à la chaine de caractères
    """
    lignes = chaine.split("\n")
    # ligne 0
    champs = lignes[0].split(separateur)
    [taille] = valider_mot_cle(champs, 0, "plateau", [True])
    # ligne 1 : le joueur
    champs = lignes[1].split(separateur)
    [id_joueur, nom_joueur, nb_points] = valider_mot_cle(
        champs, 1, "joueur", [True, False, True]
    )
    if id_joueur < 1 or id_joueur > 4:
        raise Exception("ligne 1: l'identifiant du joueur n'est pas correcte")
    # ligne 2 : l'avatar
    champs = lignes[2].split(separateur)
    [ligne_av, colonne_av] = valider_mot_cle(champs, 2, "avatar", [True, True])
    if ligne_av < 0 or ligne_av > taille or colonne_av < 0 or colonne_av > taille:
        raise Exception("ligne 2: la position de l'avatar est incorrecte")
    # ligne 3 serveur
    champs = lignes[3].split(separateur)
    [protec_serveur] = valider_mot_cle(champs, 3, "serveur", [True])
    if protec_serveur < 0:  # or protec_serveur > 4:
        raise Exception("ligne 3: la protection du serveur est incorrecte")
    # ligne 4 ordinateurs
    champs = lignes[4].split(separateur)
    [protec_ordis] = valider_mot_cle(champs, 4, "ordinateurs", [True])
    if protec_ordis < 0:  # or protec_ordis > 5:
        raise Exception("ligne 4: la protection des ordinateurs est incorrecte")
    # ligne 5 protection en cours
    champs = lignes[5].split(separateur)
    num_lig = 5
    valider_mot_cle(champs, num_lig, "trojans entrants", [])
    num_lig += 1
    num_lig, trojans_entrants = liste_trojans_from_str(lignes, num_lig, separateur)
    champs = lignes[num_lig].split(separateur)
    valider_mot_cle(champs, num_lig, "trojans sortants", [])
    num_lig += 1
    trojans_sortants = {}
    for direct in "HBDG":
        champs = lignes[num_lig].split(separateur)
        if len(champs) != 1:
            raise Exception(
                "ligne "
                + str(num_lig)
                + ": contient "
                + str(len(champs))
                + " champs au lieu de 1"
            )
        if champs[0] != direct:
            raise Exception(
                "ligne " + str(num_lig) + ": direction " + direct + " manquante"
            )
        num_lig += 1
        num_lig, liste_trojans = liste_trojans_from_str(lignes, num_lig, separateur)
        trojans_sortants[direct] = liste_trojans

    le_plateau = creer_plateau(id_joueur, nom_joueur, taille)
    ajouter_points(le_plateau, nb_points)
    case.enlever_avatar(
        matrice.get_val(get_matrice(le_plateau), taille // 2, taille // 2)
    )
    poser_avatar(le_plateau, ligne_av, colonne_av)
    set_trojans_entrants(le_plateau, trojans_entrants)
    for direct in trojans_sortants:
        set_trojans_sortants(le_plateau, direct, trojans_sortants[direct])
    equipement.set_resistance(get_serveur(le_plateau), protec_serveur)
    equipement.set_resistance(get_ordis(le_plateau), protec_ordis)
    champs = lignes[num_lig].split(separateur)
    valider_mot_cle(champs, num_lig, "matrice", [])
    num_lig += 1
    la_matrice = get_matrice(le_plateau)
    while True:
        champs = lignes[num_lig].split(separateur)
        if champs[0] != "case":
            break
        if len(champs) != 3:
            raise Exception(
                "ligne "
                + str(num_lig)
                + ": contient "
                + str(len(champs))
                + " champs au lieu de 3"
            )
        try:
            ligne = int(champs[1])
            colonne = int(champs[2])
            if ligne < 0 or ligne >= taille or colonne < 0 or colonne >= taille:
                raise Exception()
        except:
            raise Exception(
                "ligne "
                + str(num_lig)
                + ": la spécification de la case n'est pas correcte"
            )
        num_lig += 1
        champs = lignes[num_lig].split(separateur)
        protect = None
        if len(champs) != 1 and len(champs) != 3:
            raise Exception(
                "ligne " + str(num_lig) + " : la ligne doit comporter 1 ou 3 champs"
            )
        if champs[0] != "protection":
            raise Exception("ligne " + str(num_lig) + " : mot cle protection manquant")
        if len(champs) == 3:
            try:
                type_p = int(champs[1])
                res_p = int(champs[2])
                if (
                    type_p < 0
                    or type_p >= protection.PAS_DE_PROTECTION
                    or res_p > 2
                    or res_p <= 0
                ):
                    raise Exception()
                protect = protection.creer_protection(type_p, res_p)
            except:
                raise Exception(
                    "ligne " + str(num_lig) + " : la protection est mal spécifiée"
                )
        num_lig += 1
        champs = lignes[num_lig].split(separateur)
        valider_mot_cle(champs, num_lig, "trojans presents", [])
        num_lig += 1
        num_lig, trojan_cp = liste_trojans_from_str(lignes, num_lig, separateur)
        champs = lignes[num_lig].split(separateur)
        valider_mot_cle(champs, num_lig, "trojans entrants", [])
        num_lig += 1
        num_lig, trojan_ce = liste_trojans_from_str(lignes, num_lig, separateur)
        la_case = matrice.get_val(la_matrice, ligne, colonne)
        case.set_les_trojans(la_case, trojan_cp, trojan_ce)
        if protect is not None:
            case.set_protection(la_case, protect)
    champs = lignes[num_lig].split(separateur)
    valider_mot_cle(champs, num_lig, "fin plateau", [])
    return le_plateau


def sauver_plateau(plateau, nom_fic, separateur=";"):
    """sauvegarde un plateau dans un fichier

    Args:
        plateau (dict): un plateau
        nom_fic (str): le nom du fichier où sauvegarder le plateau
        separateur (str, optional): caractère séparateur utiliser dans le fichier. Defaults to ';'.
    """
    with open(nom_fic, "w") as fic:
        fic.write(plateau_2_str(plateau, separateur))


def charger_plateau(nom_fic, separateur=";"):
    """charge un plateau sauvegardé dans un fichier

    Args:
        nom_fic (str): nom du fichier
        separateur (str, optional): caractère séparateur utiliser dans le fichier. Defaults to ";".

    Returns:
        plateau: un plateau
    """
    with open(nom_fic) as fic:
        chaine = fic.read()
        return creer_plateau_from_str(chaine, separateur)


# fonctions additionnelles sur le plateau
def set_nom_joueur(plateau, nom_joueur):
    """change le nom du joueur de ce plateau

    Args:
        plateau (dict): le plateau
        nom_joueur (str): le nom du joueur
    """
    joueur.set_nom(plateau['joueur'],nom_joueur)