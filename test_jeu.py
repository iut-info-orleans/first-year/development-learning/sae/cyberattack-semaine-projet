"""
Projet CyberAttack@IUT'O
SAÉ1.01 département Informatique IUT d'Orléans 2021-2022

Module test_jeu.py
"""
import jeu
import test_plateau


# Création de jeux pour les tests
def creer_quatre_jeux():
    """
    Crée quatre jeux pour les fonctions de tests.
    """

    jeu_1 = jeu.creer_jeu(["Kylian", "Antoine", "Romain", "Jean-Baptiste"])
    jeu_2 = jeu.creer_jeu(["Jean", "Bob", "Alice", "Clara"], nb_tours_max=5)
    jeu_3 = jeu.creer_jeu(["Bob", "Kylian", "Jean-Baptiste", "Jean-Bernard"], nb_tours_max=3)
    jeu_4 = jeu.creer_jeu(["Clara", "Léna", "Bob", "Louise"], nb_tours_max=0)

    return (jeu_1, jeu_2, jeu_3, jeu_4)


# Fonctions de tests
def test_get_taille_plateau():
    (jeu_1, jeu_2, jeu_3, jeu_4) = creer_quatre_jeux()

    assert jeu.get_taille_plateau(jeu_1) == 5
    assert jeu.get_taille_plateau(jeu_2) == 5
    assert jeu.get_taille_plateau(jeu_3) == 5
    assert jeu.get_taille_plateau(jeu_4) == 5


"""
def test_get_plateau():
    (jeu_1, jeu_2, jeu_3, jeu_4) = creer_quatre_jeux()
    (plateau_1, plateau_2, plateau_3, plateau_4) = test_plateau.creer_quatre_plateaux()

    assert jeu.get_plateau(jeu_1, 4) == plateau_4
    assert jeu.get_plateau(jeu_2, 1) == plateau_1
    assert jeu.get_plateau(jeu_3, 3) == plateau_3
    assert jeu.get_plateau(jeu_4, 2) == plateau_2
"""


def test_get_num_tour():
    (jeu_1, jeu_2, jeu_3, jeu_4) = creer_quatre_jeux()

    assert jeu.get_num_tour(jeu_1) == 1
    assert jeu.get_num_tour(jeu_2) == 1
    assert jeu.get_num_tour(jeu_3) == 1
    assert jeu.get_num_tour(jeu_4) == 1


def test_get_nb_tours_max():
    (jeu_1, jeu_2, jeu_3, jeu_4) = creer_quatre_jeux()

    assert jeu.get_nb_tours_max(jeu_1) == -1
    assert jeu.get_nb_tours_max(jeu_2) == 5
    assert jeu.get_nb_tours_max(jeu_3) == 3
    assert jeu.get_nb_tours_max(jeu_4) == 0


def test_est_fini():
    (jeu_1, jeu_2, jeu_3, jeu_4) = creer_quatre_jeux()

    assert jeu.est_fini(jeu_1)
    assert not jeu.est_fini(jeu_2)
    assert not jeu.est_fini(jeu_3)
    assert jeu.est_fini(jeu_4)
